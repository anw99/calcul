package main

import (
	"calcul/theme"
	_ "embed"
	"fmt"
	"math"
	"strings"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"github.com/Knetic/govaluate"
)

const (
	ERROR = "ERROR"
)

var (
	//go:embed logo.png
	iconData []byte
	icon     = fyne.NewStaticResource("icon", iconData)

	functions = map[string]govaluate.ExpressionFunction{
		"sqrt": func(args ...interface{}) (interface{}, error) {
			value := math.Sqrt(args[0].(float64))
			return (float64)(value), nil
		},
	}
)

func evaluate(formula string) (interface{}, error) {
	expression, err := govaluate.NewEvaluableExpressionWithFunctions(formula, functions)
	if err != nil {
		return nil, err
	}
	parameters := map[string]interface{}{
		"π": 3.14159265358979323846264338327950288419716939937510582,
	}
	return expression.Evaluate(parameters)
}

func calculate(formula string, label *widget.Label) {
	formula = strings.ToLower(formula)
	formula = strings.ReplaceAll(formula, "x", "*")
	formula = strings.ReplaceAll(formula, "×", "*")
	formula = strings.ReplaceAll(formula, "÷", "/")
	formula = strings.ReplaceAll(formula, "+", "+")
	formula = strings.ReplaceAll(formula, "−", "-")
	formula = strings.ReplaceAll(formula, ",", ".")
	formula = strings.ReplaceAll(formula, "mod", "%")
	formula = strings.ReplaceAll(formula, "²", "**2")
	formula = strings.ReplaceAll(formula, "%", "/100")
	formula = strings.ReplaceAll(formula, "√", "sqrt")

	result, err := evaluate(formula)
	if err != nil {
		label.SetText(ERROR)
	} else {
		label.SetText(fmt.Sprintf("%v", result))
	}
}

func main() {
	myApp := app.New()
	myApp.Settings().SetTheme(&theme.CustomTheme{})
	myApp.SetIcon(icon)

	resultLabel := widget.NewLabel("")

	inputField := widget.NewEntry()
	inputField.OnSubmitted = func(formula string) {
		calculate(formula, resultLabel)
	}

	equalButton := widget.NewButton("=", func() {
		calculate(inputField.Text, resultLabel)
	})
	equalButton.Importance = widget.HighImportance

	divideButton := widget.NewButton("÷", func() {
		inputField.Text = inputField.Text + "÷"
		inputField.Refresh()
	})

	subtractButton := widget.NewButton("−", func() {
		inputField.Text = inputField.Text + "−"
		inputField.Refresh()
	})

	multiplyButton := widget.NewButton("×", func() {
		inputField.Text = inputField.Text + "×"
		inputField.Refresh()
	})

	addButton := widget.NewButton("+", func() {
		inputField.Text = inputField.Text + "+"
		inputField.Refresh()
	})

	decimalPointButton := widget.NewButton(",", func() {
		inputField.Text = inputField.Text + ","
		inputField.Refresh()
	})

	openingParenthesisButton := widget.NewButton("(", func() {
		inputField.Text = inputField.Text + "("
		inputField.Refresh()
	})

	closingParenthesisButton := widget.NewButton(")", func() {
		inputField.Text = inputField.Text + ")"
		inputField.Refresh()
	})

	piConstantButton := widget.NewButton("π", func() {
		inputField.Text = inputField.Text + "π"
		inputField.Refresh()
	})

	clearAllButton := widget.NewButton("AC", func() {
		inputField.Text = ""
		inputField.Refresh()
		resultLabel.SetText("")
	})

	moduloButton := widget.NewButton("mod", func() {
		inputField.Text = inputField.Text + "mod"
		inputField.Refresh()

	})

	squareRootButton := widget.NewButton("√x", func() {
		inputField.Text = inputField.Text + "√("
		inputField.Refresh()

	})

	squareButton := widget.NewButton("x²", func() {
		inputField.Text = inputField.Text + "²"
		inputField.Refresh()

	})

	inverseButton := widget.NewButton("1/x", func() {
		inputField.Text = inputField.Text + "1/"
		inputField.Refresh()

	})

	percentButton := widget.NewButton("%", func() {
		inputField.Text = inputField.Text + "%"
		inputField.Refresh()

	})

	clearAllButton.Importance = widget.DangerImportance

	digitButtons := make([]*widget.Button, 10)
	for d := 0; d <= 9; d++ {
		digit := fmt.Sprintf("%v", d)
		digitButtons[d] = widget.NewButton(digit, func() {
			inputField.Text = inputField.Text + digit
			inputField.Refresh()
		})
	}

	grid := container.NewGridWithColumns(5)
	grid.Add(clearAllButton)
	grid.Add(openingParenthesisButton)
	grid.Add(closingParenthesisButton)
	grid.Add(moduloButton)
	grid.Add(piConstantButton)
	grid.Add(digitButtons[7])
	grid.Add(digitButtons[8])
	grid.Add(digitButtons[9])
	grid.Add(divideButton)
	grid.Add(squareRootButton)
	grid.Add(digitButtons[4])
	grid.Add(digitButtons[5])
	grid.Add(digitButtons[6])
	grid.Add(multiplyButton)
	grid.Add(squareButton)
	grid.Add(digitButtons[1])
	grid.Add(digitButtons[2])
	grid.Add(digitButtons[3])
	grid.Add(subtractButton)
	grid.Add(inverseButton)
	grid.Add(digitButtons[0])
	grid.Add(decimalPointButton)
	grid.Add(percentButton)
	grid.Add(addButton)
	grid.Add(equalButton)

	w := myApp.NewWindow("Calculator")
	w.SetContent(container.NewVBox(inputField, resultLabel, grid))
	w.Canvas().Focus(inputField)
	w.Resize(fyne.Size{Width: 300, Height: 100})
	w.CenterOnScreen()
	w.ShowAndRun()
}
