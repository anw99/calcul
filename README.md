# calcul

The famous calculator based on [fyne](https://fyne.io/).

And a great way to discover go-lang for my son Antoine! His idea and his doing with my help.

## Support

[open an issue here](https://gitlab.com/arno750/calcul/-/issues).

## Authors & contributors

Original setup of this repository by [Arnaud WIELAND](https://gitlab.com/arno750) and Antoine WIELAND.

## License

Every details about licence are in a [separate document](LICENSE).

## About the project

### Screenshots

![](doc/screenshot-1.png)

![](doc/screenshot-2.png)

### Build

> [install Go](https://go.dev/doc/install)
> 
> [install fyne ](https://developer.fyne.io/started/)

```bash
go build
```

### Run

```bash
./calcul
```

