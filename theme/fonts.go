package theme

import (
	_ "embed"

	"fyne.io/fyne/v2"
)

var (
	//go:embed Ubuntu-B.ttf
	fontB            []byte
	fontBoldResource = fyne.NewStaticResource("Ubuntu-B font", fontB)

	//go:embed Ubuntu-R.ttf
	fontR               []byte
	fontRegularResource = fyne.NewStaticResource("Ubuntu-R font", fontR)
)
