.DEFAULT_GOAL := build
.PHONY: help echo build lint test clean all

PACKAGE_NAME ?= export-md
VERSION ?= $(shell git describe --tags)

help: ## This help message
	@echo "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\033[1;34m\1\\033[0;39m:\2/' | column -c2 -t -s :)"

format: ## Format code with gofmt
	gofmt -w .

echo: ## Display the package name and version
	@echo ${PACKAGE_NAME}-${VERSION}

mock: ## Build mock (and install mockgen beforehand if necessary)
	if [ ! -f ~/go/bin/mockgen ]; then go install github.com/golang/mock/mockgen@latest; fi
	go generate ./...

build: mock ## Build the application
	go build

lint: ## Lint the application
	docker run --rm -it -w "/app" -v "${PWD}":/app golangci/golangci-lint:v1.50.1 golangci-lint run -v -p bugs -p error

test: ## Run non-regression test
	go test -cover ./...

clean: ## Clean
	rm -f ${PACKAGE_NAME}
	rm -rf output
	find . -type f -name "mock*.go" -delete

all: test build
